<?php
/**
 * Created by PhpStorm.
 * User: tasselchof
 * Date: 26.11.15
 * Time: 4:04
 */

require_once('orderadmin.php');

?>
<h1>Загрузка</h1>
<p>Полученный от клиента файл сохраните в формате CSV и загрузите через форму ниже.</p>
<form enctype="multipart/form-data" action="" method="POST">
    <p>Файл: <input name="userfile" type="file" /></p>
    <p>Пароль: <input name="password" type="password" /></p>
    <input type="submit" value="Обработать" />
</form>

<?

$api = new OrderadminAPI('publicKey', 'secret');

if($_FILES['userfile']['size'] > 0) {
    $i = 0;
    if (($handle = fopen($_FILES['userfile']['tmp_name'], "r")) !== FALSE) {
        echo '<pre>';
        while (($data = fgetcsv($handle, 1500, ";")) !== FALSE) {
            if ($i == 0 || empty($data[18])) {
                $i++;
                continue;
            }

            $sender = explode('ИНН', $data[2]);
            $senderInfo = explode(',', $sender[1]);

            $request = array(
                'deliveryService' => array(
                    'id' => 9,
                ),
                'currency' => array(
                    'code' => 'RUB',
                ),
                'extId' => $data[18],
                'extType' => 'delivery_request',
                'from' => array(
                    'sender' => 4,
                    'senderName' => trim($sender[0]),
                    'senderProfile' => array(
                        'name' => $sender[0],
                        'phone' => $data[0],
                        'type' => 'legal',
                        'extId' => $data[20],
                        'address' => array(
                            'country' => array(
                                'id' => 28
                            ),
                            'postcode' => $senderAddr->postal_code,
                            'locality' => array(
                                'name' => empty($senderAddr->city) ? $senderAddr->settlement : $senderAddr->city,
                                'type' => empty($senderAddr->city_type) ? $senderAddr->settlement_type : $senderAddr->city,
                            ),
                            'streetPrefix' => $senderAddr->street_type,
                            'street' => $senderAddr->street,
                            'house' => $senderAddr->house,
                            'block' => $senderAddr->block,
                            'apartment' => $senderAddr->flat,
                            'notFormal' => $data[3],
                        ),
                    ),
                ),
                'to' => array(
                    'recipientName' => $data[11],
                    'recipient' => array(
                        'name' => $data[11],
                        'type' => 'physical',
                        'address' => array(
                            'country' => array(
                                'id' => 28
                            ),
                            'postcode' => $data[8],
                            'locality' => array(
                                'name' => empty($recipientAddr->city) ? $recipientAddr->settlement : $recipientAddr->city,
                                'type' => empty($recipientAddr->city_type) ? $recipientAddr->settlement_type : $recipientAddr->city,
                            ),
                            'streetPrefix' => $recipientAddr->street_type,
                            'street' => $recipientAddr->street,
                            'house' => $recipientAddr->house,
                            'block' => $recipientAddr->block,
                            'apartment' => $recipientAddr->flat,
                            'notFormal' => $data[9],
                        ),
                    ),
                ),
                'estimatedCost' => floatval($data[12]),
                'payment' => floatval($data[13]),
                'weight' => $data[15],
            );

            $response = $api->setRequest($request)->request('POST', '/delivery-services/request')->getResult();
            $res = json_decode($response);

            if ($res->status) {
                echo $request['extId'] . ': ' . $res->detail . "\n";
            } else {
                echo $request['extId'] . ': ' . $res->message . "\n";
            }
            if ($_REQUEST['debug']) {
                print_r($res);
            }
        }
        echo '</pre>';
        fclose($handle);
    }
}
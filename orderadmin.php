<?php

/**
 * Created by PhpStorm.
 * User: tasselchof
 * Date: 14.09.15
 * Time: 0:35
 */
class OrderadminAPI
{
    static $MODULE_ID = "iqcreative.orderadmin";

    protected $publicKey;
    protected $secret;
    protected $key;
    protected $json;
    protected $result;
    protected $timeout = 15;
    protected $error;

    public function __construct($publicKey, $secret)
    {
        $this->publicKey = $publicKey;
        $this->key = hash_hmac('sha1', date('Y-m-d'), $secret);
    }

    public function getResult()
    {
        return $this->result;
    }

    public function getError()
    {
        return $this->error;
    }

    public function setRequest($array)
    {
        $this->json = json_encode($array);

        return $this;
    }

    public function setJsonRequest($json)
    {
        $this->json = $json;

        return $this;
    }

    public function getRequest()
    {
        return $this->json;
    }

    public function request($type, $url, $debug = false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://beta.orderadmin.ru/api/rest/latest/delivery-services/request');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getRequest());
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: OAD ' . $this->publicKey . ':' . $this->key,
            'Accept: application/json',
        ));

        $this->result = curl_exec($ch);

        if($debug) {
            echo $this->result;

            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLINFO_HEADER_OUT, true);
            echo '<pre>';
            print_r(curl_getinfo($ch));
            echo '</pre>';

            die();
        }

        curl_close($ch);

        return $this;
    }
}